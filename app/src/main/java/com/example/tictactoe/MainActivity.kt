package com.example.tictactoe

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var firstPlayer = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttons()
        reset()


    }

    private fun buttons() {
        button00.setOnClickListener(this)
        button01.setOnClickListener(this)
        button02.setOnClickListener(this)
        button10.setOnClickListener(this)
        button11.setOnClickListener(this)
        button12.setOnClickListener(this)
        button20.setOnClickListener(this)
        button21.setOnClickListener(this)
        button22.setOnClickListener(this)







    }
    private fun reset(){
        buttonreset.setOnClickListener(){
            button00.text = ""
            button00.isClickable = true
            button01.text = ""
            button01.isClickable = true
            button02.text = ""
            button02.isClickable = true
            button10.text = ""
            button10.isClickable = true
            button11.text = ""
            button11.isClickable = true
            button12.text = ""
            button12.isClickable = true
            button20.text = ""
            button20.isClickable = true
            button21.text = ""
            button21.isClickable = true
            button22.text = ""
            button22.isClickable = true

        }
    }

    private fun buttonchange(button: Button) {
        if (firstPlayer) {
            button.text = "X"
        } else {
            button.text = "0"
        }
        button.isClickable = false
        firstPlayer = !firstPlayer
        winner()


    }

    override fun onClick(v: View?) {
        buttonchange(v as Button)

    }

    private fun winner() {
        if (button00.text.toString()
                .isNotEmpty() && button00.text.toString() == button01.text.toString() && button01.text.toString() == button02.text.toString()
        ) {
            showToast(button00.text.toString())
            stopbutton(button10)
            stopbutton(button11)
            stopbutton(button12)
            stopbutton(button20)
            stopbutton(button21)
            stopbutton(button22)
            startActivity(getIntent());
        } else if (button10.text.toString()
                .isNotEmpty() && button10.text.toString() == button11.text.toString() && button11.text.toString() == button12.text.toString()
        ) {
            showToast(button10.text.toString())
            stopbutton(button00)
            stopbutton(button01)
            stopbutton(button02)
            stopbutton(button20)
            stopbutton(button21)
            stopbutton(button22)
        } else if (button20.text.toString()
                .isNotEmpty() && button20.text.toString() == button21.text.toString() && button21.text.toString() == button22.text.toString()
        ) {
            showToast(button20.text.toString())
            stopbutton(button00)
            stopbutton(button01)
            stopbutton(button02)
            stopbutton(button10)
            stopbutton(button11)
            stopbutton(button12)
        } else if (button00.text.toString()
                .isNotEmpty() && button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString()
        ) {
            showToast(button00.text.toString())
            stopbutton(button01)
            stopbutton(button11)
            stopbutton(button21)
            stopbutton(button02)
            stopbutton(button12)
            stopbutton(button22)
        } else if (button01.text.toString()
                .isNotEmpty() && button01.text.toString() == button11.text.toString() && button11.text.toString() == button21.text.toString()
        ) {
            showToast(button01.text.toString())
            stopbutton(button00)
            stopbutton(button10)
            stopbutton(button20)
            stopbutton(button02)
            stopbutton(button12)
            stopbutton(button22)
        } else if (button02.text.toString()
                .isNotEmpty() && button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString()
        ) {
            showToast(button02.text.toString())
            stopbutton(button00)
            stopbutton(button10)
            stopbutton(button20)
            stopbutton(button01)
            stopbutton(button11)
            stopbutton(button21)
        } else if (button00.text.toString()
                .isNotEmpty() && button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString()
        ) {
            showToast(button00.text.toString())
            stopbutton(button01)
            stopbutton(button02)
            stopbutton(button10)
            stopbutton(button12)
            stopbutton(button20)
            stopbutton(button21)
        } else if (button02.text.toString()
                .isNotEmpty() && button02.text.toString() == button11.text.toString() && button11.text.toString() == button20.text.toString()
        ) {
            showToast(button02.text.toString())
            stopbutton(button00)
            stopbutton(button01)
            stopbutton(button10)
            stopbutton(button12)
            stopbutton(button21)
            stopbutton(button22)
        } else if (button00.text.toString().isNotEmpty() && button01.text.toString()
                .isNotEmpty() && button02.text.toString().isNotEmpty()
            && button10.text.toString().isNotEmpty() && button11.text.toString()
                .isNotEmpty() && button12.text.toString().isNotEmpty()
            && button20.text.toString().isNotEmpty() && button21.text.toString()
                .isNotEmpty() && button22.text.toString().isNotEmpty()
        )
            Toast.makeText(applicationContext, "the outcome is tie", Toast.LENGTH_SHORT).show()


    }

    private fun stopbutton(button: Button) {
        button.isClickable = false

    }

    private fun showToast(message: String) {
        Toast.makeText(this, "the winner is $message", Toast.LENGTH_SHORT).show()
    }


}







